function estPalindrome(chaine) {
    // Supprimer les espaces, la ponctuation et passer en minuscules
    chaine = chaine.replace(/[^a-z0-9]/g, '').toLowerCase();
  
    // Cas de base : la chaîne est vide ou ne contient qu'un caractère
    if (chaine.length <= 1) {
      return true;
    }
  
    // Comparer le premier et le dernier caractère
    if (chaine.charAt(0) === chaine.charAt(chaine.length - 1)) {
      // Appel récursif avec la sous-chaîne sans le premier et le dernier caractère
      return estPalindrome(chaine.slice(1, chaine.length - 1));
    } else {
      return false;
    }
  }