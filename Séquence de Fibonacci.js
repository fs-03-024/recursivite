function fibonacci(n) {
    // Cas de base
    if (n <= 1) {
      return n;
    }
    // Cas récursif
    return fibonacci(n - 1) + fibonacci(n - 2);
  }