function puissance(base, exposant) {
    // Cas de base : exposant = 0
    if (exposant === 0) {
      return 1;
    }
    
    // Cas récursif : exposant > 0
    return base * puissance(base, exposant - 1);
  }